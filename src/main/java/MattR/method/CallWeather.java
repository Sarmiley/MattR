package MattR.method;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import MattR.utils.ExcutePostJson;

public class CallWeather {
	
	
	public String callWeatherApi(String city){
		System.err.println("========= 執行查詢天氣Api 城市：" + city + " =========");
		String resultStr = "";
		String weatherApiUrl = "https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?";
		String authKey = "Authorization=CWB-156D789D-DFD3-47F2-808A-5F082DC722C0";
		String locationName = "locationName=";
		String apiUrl = "";
		if("".equals(city) || city == null){
			apiUrl = weatherApiUrl + "&" + authKey + "&" + locationName + "新北市";
		}else{
			apiUrl = weatherApiUrl + "&" + authKey + "&" + locationName + city;
		}
		resultStr = ExcutePostJson.excuteGetJson(apiUrl);
		System.err.println(apiUrl);
		System.err.println(resultStr);
		return resultStr;
	}
	
	public String callWeatherApiForLoc(String city,String locName){
		String resultStr		= "";
		String weatherApiUrl	= "https://opendata.cwb.gov.tw/api/v1/rest/datastore/";
//		String cityCode			=  city + "?";
		String locationName		= "locationName=";
		String authKey			= "Authorization=CWB-156D789D-DFD3-47F2-808A-5F082DC722C0";
		String timeFrom			= "timeFrom=";
		String timeTo			= "timeTo=";
		String searchFields		= "elementName=PoP6h,Wx,CI,T";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		Calendar cal = Calendar.getInstance();
		String[] timeFromAry =  sdf1.format(cal.getTime()).split(" ");
		timeFrom =  timeFrom + timeFromAry[0]+ "T" + timeFromAry[1];
		cal.add(Calendar.HOUR_OF_DAY, 6);
		String[] timeToAry =  sdf1.format(cal.getTime()).split(" ");
		timeTo = timeTo +  timeToAry[0]+ "T" + timeToAry[1];
		
		
		
		String apiUrl = "";
		
		if("".equals(locName) || locName == null){
			apiUrl = weatherApiUrl+ "F-D0047-069?" + "&" + authKey + "&" + locationName + "板橋區" + "&" + timeFrom + "&" + timeTo + "&" + searchFields;
		}else{
			apiUrl = weatherApiUrl+ "F-D0047-069?" + "&" + authKey + "&" + locationName + "板橋區" + "&" + timeFrom + "&" + timeTo + "&" + searchFields;
		}
		System.err.println(apiUrl);
		
		return resultStr;
	}
}
