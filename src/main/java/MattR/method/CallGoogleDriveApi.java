package MattR.method;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import MattR.utils.GoogleDriveUtils;
import MattR.utils.MimeTypeConstants;

public class CallGoogleDriveApi {

	private static Drive driveService;

	static {
		if (driveService == null) {
			try {
				driveService = GoogleDriveUtils.getDriveService();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<File> getDriveFolderMap(String googleFolderIdParent) {
		String pageToken = null;
		List<File> list = new ArrayList<File>();

		String query = null;
		if (googleFolderIdParent == null) {
			query = " mimeType = 'application/vnd.google-apps.folder' " //
					+ " and 'root' in parents";
		} else {
			query = " mimeType = 'application/vnd.google-apps.folder' " //
					+ " and '" + googleFolderIdParent + "' in parents";
		}

		do {
			FileList result = null;
			try {
				result = driveService.files().list().setQ(query).setSpaces("drive") //
						.setFields("nextPageToken, files(id, name)")//
						.setPageToken(pageToken).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (File file : result.getFiles()) {
				list.add(file);
			}
			pageToken = result.getNextPageToken();
		} while (pageToken != null);
		return list;
	}

	// 利用資料夾名稱取得資料夾ID
	public static String getDriveFolderByName(String folderName) {
		String pageToken = null;
		String folderId = "";
		List<File> list = new ArrayList<File>();
		String query = " name = '" + folderName + "' " //
				+ " and mimeType = 'application/vnd.google-apps.folder' ";
		do {
			FileList result = null;
			try {
				result = driveService.files().list().setQ(query).setSpaces("drive") //
						// Fields will be assigned values: id, name,
						// createdTime,
						// mimeType
						.setFields("nextPageToken, files(id, name, createdTime, mimeType)")//
						.setPageToken(pageToken).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (result.getFiles().size() > 0) {
				for (File file : result.getFiles()) {
					list.add(file);
				}
				folderId = list.get(0).getId();
			}
			pageToken = result.getNextPageToken();
		} while (pageToken != null);
		return folderId;
	}

	// 利用資料夾名稱及副檔名取得檔案清單
	public static List<File> getFilesListByFolderName(String folderId, String ext) {

		String pageToken = null;
		List<File> list = new ArrayList<File>();
		String query = "'" + folderId + "' in parents  " + " and trashed = false";
		if (ext == null || "".equals(ext)) {
			query += " and mimeType != 'application/vnd.google-apps.folder' ";
		} else {
			query += " and mimeType = '" + MimeTypeConstants.getMimeType(ext.replace(".", "")) + "'";
		}
		do {
			FileList result = null;
			try {
				result = driveService.files().list().setQ(query).setSpaces("drive") //
						.setFields("nextPageToken, files(name)")//
						.setPageToken(pageToken).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.err.println("共有 " + result.getFiles().size() + " 個檔案");
			for (File file : result.getFiles()) {
				list.add(file);
			}
			pageToken = result.getNextPageToken();
		} while (pageToken != null);
		return list;
	}

	
	//用關鍵字取得檔案連結
	public static List<File> getAVFilesContent(String keyword , String folderId , String ext, String size) {

		String pageToken = null;
		List<File> list = new ArrayList<File>();
		String query = "name contains '" + keyword + "'" + " and trashed = false";
		int sizeInt = 30;
		if(ext != null && !("".equals(ext))){
			query += " and mimeType = '" + MimeTypeConstants.getMimeType(ext.replace(".", "")) + "'";
		}else{
			query +=  " and mimeType != 'application/vnd.google-apps.folder'";
		}

		if(folderId != null && !("".equals(folderId))){
			query += " and '" + folderId + "' in parents  ";
		}
		
		
		FileList result = null;
		try {
			System.err.println(query);
			if(size == null){
				result = driveService.files().list().setQ(query).setSpaces("drive").setPageSize(30)
						.setSupportsTeamDrives(false)
						.setOrderBy("name") 
						.setFields("nextPageToken, files(name,webViewLink)")//
						.setPageToken(pageToken).execute();
			}else{
				sizeInt = Integer.parseInt(size);
				if(sizeInt > 100){
					sizeInt = 100;
				}
				result = driveService.files().list().setQ(query).setSpaces("drive").setPageSize(sizeInt)
						.setSupportsTeamDrives(false)
						.setOrderBy("name") 
						.setFields("nextPageToken, files(name,webViewLink)")//
						.setPageToken(pageToken).execute();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("共有 " + result.getFiles().size() + " 個檔案");
		for (File file : result.getFiles()) {
			list.add(file);
		}
		return list;
	}
}
