/**
 * 方法選擇器
 * @author Matt Liu
 * @since 2018/08/16
 * @version 1.0
 */

package MattR.method;

import java.io.IOException;

import MattR.data.MattRSpeakVo;
import MattR.gdrive_method.SelectGdDriveMethod;
import MattR.service.MattRSpeakService;
import MattR.service.MattRWeatherService;
import MattR.utils.LearnStringSplit;

public class MethodsCheckController {
	
	public static final String WEATHER					= "天氣";
	public static final String WEATHER36				= "天氣36";
	public static final String WEATHER_LOC				= "天氣Loc";
	public static final String LEARN_SPEAK				= "學";
	public static final String FORGOT					= "忘";
	public static final String REPLY					= "回應";
	public static final String FIND_AV					= "找檔案";
	public static final String FIND_AV_LIST				= "檔案清單";
	public static final String FIND_FOLDER_LIST			= "資料夾清單";
	public static final String FIND_FILES_LIST_FOLDER	= "資料夾內容";
	
	private static MattRSpeakService mss;
	private static SelectGdDriveMethod sgdm;
	
	public MethodsCheckController() throws IOException{
		if(mss == null){
			mss = new MattRSpeakService();
		}
		if(sgdm == null){
			sgdm = new SelectGdDriveMethod();
		}
	}
	
	/**
	 * 利用輸入的字串，擷取開頭文字判斷執行方法，並在執行之後回傳結果
	 * @param inputMsg
	 * @return String
	 * @author Matt Liu
	 * @since 2018/08/16
	 * @version 1.0
	 * @throws Exception 
	 */
	public String methodCheck(String inputMsg){
		
		System.err.println("=============== methodCheck START ================");
		String methodName = "";
		String resultMsg = "";
		String[] msgAry = inputMsg.split("~");
		String method = msgAry[0];
		
		//查詢當前天氣
		if(WEATHER.equals(method)){
			methodName =  WEATHER;
			MattRWeatherService mrws = new MattRWeatherService();
			resultMsg = mrws.searchWeatherbrCounty(WEATHER, msgAry[1]);
		
		//查詢36小時天氣
		}else if(WEATHER36.equals(method)){
			methodName =  WEATHER36;
			MattRWeatherService mrws = new MattRWeatherService();
			resultMsg = mrws.searchWeatherbrCounty(WEATHER36, msgAry[1]);
		
		//學說話
		}else if(LEARN_SPEAK.equals(method)){
			methodName =  LEARN_SPEAK ;
			String[] learnMsg = LearnStringSplit.learnForgotSplit(inputMsg);
			MattRSpeakVo vo = new MattRSpeakVo();
			if(learnMsg.length >= 2){
				vo.setMsgKey(learnMsg[0]);
				vo.setReplyMsg(learnMsg[1]);
				resultMsg = mss.insertSpeak(vo);
			}
			
		//忘記說話
		}else if(FORGOT.equals(method)){
			methodName = FORGOT;
			String[] forgotMsg = LearnStringSplit.learnForgotSplit(inputMsg);
			if(forgotMsg[0] != null){
				resultMsg = mss.setMsgStatus(forgotMsg[0]);
			}
		
		//找片
		}else if(FIND_AV.equals(method)){
			methodName = FIND_AV;
			//關鍵字
			String[] input = inputMsg.replace(FIND_AV, "").split(";");
			String keyword = input[0];
			String folder = input.length > 1 ? ("".equals(input[1]) ? null : input[1]) : null;
			String ext =    input.length > 2 ? ("".equals(input[2]) ? null : input[2]) : null;
			String size =   input.length > 3 ? ("".equals(input[3]) ? null : input[3]) : "15";
			//最大筆數 不填 =不限
			resultMsg = sgdm.getAVFilesContent(keyword,folder,ext,size);
		//找片清單
		}else if(FIND_AV_LIST.equals(method)){
			methodName = FIND_AV_LIST;
			//關鍵字
			String[] input = inputMsg.replace(FIND_AV_LIST, "").split("-");
			String keyword = input[0];
			String folder = "".equals(input[1]) ? null : input[1];
			String ext = "".equals(input[2]) ? null : input[2];
			String size = "".equals(input[3]) ? null : input[3];
			//最大筆數 不填 =不限
			resultMsg = sgdm.getAVFilesList(keyword,folder,ext,size);
		//找資料夾
		}else if(FIND_FOLDER_LIST.equals(method)){
			methodName = FIND_FOLDER_LIST;
			if(msgAry.length > 1){
				resultMsg = sgdm.getDriveFolderList(SelectGdDriveMethod.getTopNodeFolderIdByName(msgAry[1]));
			}else{
				resultMsg = sgdm.getDriveRootFolderMap();
			}
		}else if(FIND_FILES_LIST_FOLDER.equals(method)){
			methodName = FIND_FILES_LIST_FOLDER;
			String ext = null;
			if(msgAry.length>2){
				ext = msgAry[2];
			}
			resultMsg = sgdm.getFilesListByFolderName(msgAry[1],ext);
		}else{
			methodName = REPLY;
			resultMsg = mss.queryReplyMsg(inputMsg);
		}
		System.err.println("執行的功能是： " + methodName);
		System.err.println("執行的結果是： " + resultMsg);
		System.err.println("=============== methodCheck END ================");
		return resultMsg;
	}
}
