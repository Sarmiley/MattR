package MattR.method;

import MattR.utils.ResultTransferUtils;

public class LineBotAppController2 {
	private String keyword;
	private String city;
	
	private static CallWeather callweather;
	
	
	public LineBotAppController2(){
		if(callweather == null){
			callweather = new CallWeather();
		}
	}
	
	
	public String selectMethod(){
		String resultStr = "";
		String messageStr = "";
		if("天氣".equals(keyword)){
			System.err.println("========= 開始查詢天氣 =========");
			resultStr = callweather.callWeatherApi(city);
			messageStr = ResultTransferUtils.weatherResult2Message(resultStr);
			callweather.callWeatherApiForLoc("F-D0047-061","內湖區");
		}else if("天氣36".equals(keyword)){
			System.err.println("========= 開始查詢36小時天氣 =========");
			resultStr = callweather.callWeatherApi(city);
			messageStr = ResultTransferUtils.weatherResult2Message36hr(resultStr);
		}
		return messageStr;
	}


	public String getKeyword() {
		return keyword;
	}


	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}
	
	
}
