/**
 * Line Bot 接收事件入口程式
 * @author Matt Liu
 * @since 2018/08/16
 * @version 1.0
 */
package MattR.application;


import java.util.concurrent.ExecutionException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import MattR.controller.LineBotAppController;

@SpringBootApplication
@RestController
@LineMessageHandler
@ComponentScan(basePackages = {"MattR.controller", "MattR.service","MattR.dao","MattR.data","MattR.utils","MattR.gdrive_method" })
public class LineBotAppApplication {
	
	//功能選擇控制器
	private static LineBotAppController lbac;
	public static JdbcTemplate jdbcTemplate;
	private static ApplicationContext ctx;
	private final String token = "8q4REUOreyL3Ey7BFZl5sG3lHGQmWPJS8SS0YyuQG6M4eODjhxNREfQ89EMjxOM6Mv8tz1PGjuzbixE6zidOgZe3NiS4b7gYDb3iwwmPuCYWvAJIm6/FiWRTpHkI0yjaatKrJzKUmypcnmg6zHP4QgdB04t89/1O/w1cDnyilFU=";
	
	public static void main(String[] args) throws Exception {
		ctx = SpringApplication.run(LineBotAppApplication.class, args);
		jdbcTemplate = ctx.getBean(JdbcTemplate.class);
		if(lbac == null){
			lbac = new LineBotAppController();
		}
		
	}
	
	

	@EventMapping
	public Object handleTextMessageEvent(MessageEvent<TextMessageContent> event){
		System.err.println("=====  handleTextMessageEvent   Start ======");
		if(jdbcTemplate == null){
			jdbcTemplate = ctx.getBean(JdbcTemplate.class);
		}
		//擷取抓到的訊息
		String msgText = event.getMessage().getText();
		String userId = event.getSource().getUserId();
		//console紀錄
		System.err.println("=====================================================");
		System.err.println("取得得訊息是：" + event);
		System.err.println("=====================================================");
		System.err.println("內容為：" + msgText);
		System.err.println("=====================================================");
		//取得回復訊息
		
		String commandResult =  lbac.replyMessage(msgText);
		int resultLen = commandResult.length();
		
		if(resultLen > 2000){
			for(int lenth = 0 ; lenth < resultLen;lenth+=2000){
				int subEnd = lenth + 2000;
				if(subEnd > resultLen){
					subEnd = resultLen;
				}
				pushMessage(userId,commandResult.substring(lenth, subEnd));
			}
			return "";
		}
		
		return new TextMessage(commandResult);
	}

	@EventMapping
	public TextMessage handleDefaultMessageEvent(Event event) {
		System.out.println("=====================================================");
		System.out.println("取得非訊息事件 : " + event);
		System.out.println("=====================================================");
		return new TextMessage("這個我看不懂也聽不懂耶～");
	}
	
	
	//推送訊息
	public void pushMessage(String userId,String message){
		LineMessagingClient client = LineMessagingClient.builder(token).build();
		PushMessage pushMessage = new PushMessage(userId,new TextMessage(message));
		try {
			BotApiResponse botApiResponse = client.pushMessage(pushMessage).get();
		} catch (InterruptedException | ExecutionException e) {
		}
	}
	
}