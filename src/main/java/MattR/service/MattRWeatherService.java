package MattR.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import MattR.dao.CityDistDao;
import MattR.method.CallWeather;
import MattR.method.MethodsCheckController;
import MattR.utils.ResultTransferUtils;

@Service
public class MattRWeatherService {
	
	
	private static CallWeather callweather;
	
	@Autowired
	private CityDistDao cityDistDao;

	
	public MattRWeatherService(){
		if(callweather == null){
			callweather = new CallWeather();
		}
		if(cityDistDao == null){
			cityDistDao = new CityDistDao();
		}
	}
	
	public String searchWeatherbrCounty(String keyword , String city){
		String resultStr = "";
		String messageStr = "";
		city = city.replace("台","臺");
		if((MethodsCheckController.WEATHER).equals(keyword)){
			System.err.println("========= 開始查詢天氣 ========= city = " + city);
			List<Map<String,Object>> cityList = cityDistDao.selectCityDistByCity(city);
			if(cityList.size() > 0){
				Map<String,Object> item= cityList.get(0);
				if(item.containsKey("CITY_NAME")){
					city = item.get("CITY_NAME").toString();
				}
			}else{
				return "嗯嗯嗯？我看不到天氣預報啊啊啊啊";
			}
			resultStr = callweather.callWeatherApi(city);
			messageStr = ResultTransferUtils.weatherResult2Message(resultStr);
		}else if((MethodsCheckController.WEATHER36).equals(keyword)){
			System.err.println("========= 開始查詢36小時天氣 =========");
			List<Map<String,Object>> cityList = cityDistDao.selectCityDistByCity(city);
			if(cityList.size() > 0){
				Map<String,Object> item= cityList.get(0);
				if(item.containsKey("CITY_NAME")){
					city = item.get("CITY_NAME").toString();
				}
			}else{
				return "嗯嗯嗯？我看不到天氣預報啊啊啊啊";
			}
			resultStr = callweather.callWeatherApi(city);
			messageStr = ResultTransferUtils.weatherResult2Message36hr(resultStr);
		}
//		callweather.callWeatherApiForLoc("F-D0047-061","內湖區");
		return messageStr;
	}
	
	
}
