package MattR.service;

import org.springframework.stereotype.Service;

import MattR.dao.MattRSpeakDao;
import MattR.data.MattRSpeakVo;

@Service
public class MattRSpeakService {

	private MattRSpeakDao mattRSpeakDao;
	
	public MattRSpeakService(){
		if(mattRSpeakDao == null){
			mattRSpeakDao = new MattRSpeakDao();
		}
	}
	
	public String insertSpeak(MattRSpeakVo mattRSpeakVo){
		int count = mattRSpeakDao.insertSpeak(mattRSpeakVo);
		if(count >0){
			return  "我學會囉！";
		}else{
			return  "我居然學不會，一定哪裡出問題了！";
		}
	}
	
	public String queryReplyMsg(String keyword){
		return mattRSpeakDao.queryReplyMsg(keyword);
	}
	
	public String setMsgStatus(String keyword){
		int count =  mattRSpeakDao.setMsgStatus(keyword);
		if(count >0){
			return  "給我一杯忘～情～水～  (咕嚕) \r\n忘光啦～";
		}else{
			return  "我傻傻的，根本不會這句話啊～";
		}
	}
	
}
