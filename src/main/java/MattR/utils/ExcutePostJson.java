package MattR.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

/**
 * @author Matt
 * @since 2018年6月13日
 * @description 
 */
public class ExcutePostJson {
	public static int maxTimeOutErrorNum = 3;
	public static int maxTimeOut = 10000;
	public static int register_maxTimeOut = 35000;
	public static int send_EmailSMS_time = 180;
	public static int err_network = 0;
     
	public static String excutePostJson(String targetURL, JSONObject post_jsonObject) {
        String result = "";

        if(targetURL.contains("https:")) {
            try {

                URL url = new URL(targetURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                if(targetURL.indexOf("register") > 0){
                    conn.setReadTimeout(10 * 1000);// 設定timeout時間
                    conn.setConnectTimeout(30 * 1000);
                }else{
                    conn.setReadTimeout(10 * 1000);// 設定timeout時間
                    conn.setConnectTimeout(30 * 1000);
                }
                String input = post_jsonObject.toString();

                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                while ((output = br.readLine()) != null) {
                    result += output;
                }

                conn.disconnect();

            } catch (IOException e) {

                e.printStackTrace();
                err_network += 1;
            } catch (Exception e) {
                e.printStackTrace();
                err_network += 1;
            }
        } else {
            try {

                URL url = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                if(targetURL.indexOf("register") > 0){
                    conn.setReadTimeout(register_maxTimeOut);// 設定timeout時間
                    conn.setConnectTimeout(register_maxTimeOut);
                }else{
                    conn.setReadTimeout(maxTimeOut);// 設定timeout時間
                    conn.setConnectTimeout(maxTimeOut);
                }
                String input = post_jsonObject.toString();

                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                while ((output = br.readLine()) != null) {
                    result += output;
                }

                conn.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
                err_network += 1;
            } catch (Exception e) {
                e.printStackTrace();
                err_network += 1;
            }
        }
        return result;
    }
	
	public static String excuteGetJson(String targetURL) {
        String result = "";

       
        
        if(targetURL.contains("https:")) {
            try {

                URL url = new URL(targetURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                if(targetURL.indexOf("register") > 0){
                    conn.setReadTimeout(10 * 1000);// 設定timeout時間
                    conn.setConnectTimeout(30 * 1000);
                }else{
                    conn.setReadTimeout(10 * 1000);// 設定timeout時間
                    conn.setConnectTimeout(30 * 1000);
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;
                while ((output = br.readLine()) != null) {
                    result += output;
                }
                conn.disconnect();

            } catch (IOException e) {

                e.printStackTrace();
                err_network += 1;
            } catch (Exception e) {
                e.printStackTrace();
                err_network += 1;
            }
        } else {
            try {

                URL url = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                if(targetURL.indexOf("register") > 0){
                    conn.setReadTimeout(register_maxTimeOut);// 設定timeout時間
                    conn.setConnectTimeout(register_maxTimeOut);
                }else{
                    conn.setReadTimeout(maxTimeOut);// 設定timeout時間
                    conn.setConnectTimeout(maxTimeOut);
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;
                while ((output = br.readLine()) != null) {
                    result += output;
                }

                conn.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
                err_network += 1;
            } catch (Exception e) {
                e.printStackTrace();
                err_network += 1;
            }
        }
        return result;
    }
	
}
