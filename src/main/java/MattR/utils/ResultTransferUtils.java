package MattR.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import MattR.data.WeatherVo;

public class ResultTransferUtils {
	private static final String WEATHER_ERROR = "查天氣失敗了啊啊啊啊";
	
	public static String weatherResult2Message36hr(String resultStr){
		System.err.println("============ weatherResult2Message36hr START ============== str = " +resultStr);
		JSONObject json = null;
		StringBuffer sb = new StringBuffer();
		String br = "\r\n";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy年MM月dd日  HH點mm分");
		try {
			json = new JSONObject(resultStr);
			JSONObject records = null;
			JSONArray location = null;
			if(json.has("success")){
				if("true".equals(json.getString("success"))){
					if(json.has("records")){
						records = json.getJSONObject("records");
					}else{
						return WEATHER_ERROR;
					}
					if(records.has("location")){
						location = records.getJSONArray("location");
					}else{
						return "嗯嗯？？找不到城市的資料耶，是不是飛到火星去啦？";
					}
					
					sb.append(records.getString("datasetDescription") +br +br);
					if(location.length() == 0){
						return "嗯嗯？？找不到城市的資料耶，是不是飛到火星去啦？";
					}
					for(int i = 0;i < location.length() ; i++){
						JSONObject item = location.getJSONObject(i);
						
						//取得城市名稱
						sb.append(item.getString("locationName") + br + br +br);
						sb.append("-------------------------" + br + br);
						//取得氣象資料
						JSONArray weatherAry = item.getJSONArray("weatherElement");
						for(int j = 0 ; j < weatherAry.length() ; j ++){
							JSONObject weatherItem = weatherAry.getJSONObject(j);
							
							String elementName = weatherItem.getString("elementName");
							String elementPre = "";
							if("Wx".equalsIgnoreCase(elementName)){
								elementPre = "天氣現象：";
							}else if("PoP".equalsIgnoreCase(elementName)){
								elementPre = "降雨機率：";
							}else if("CI".equalsIgnoreCase(elementName)){
								elementPre = "舒適度：";
							}else if("MinT".equalsIgnoreCase(elementName)){
								elementPre = "最低溫度：";
							}else if("MaxT".equalsIgnoreCase(elementName)){
								elementPre = "最高溫度：";
							}
							sb.append(elementPre +br);
							
							JSONArray times = weatherItem.getJSONArray("time");
							for(int k = 0 ; k <times.length() ; k++ ){
								JSONObject timeObj = times.getJSONObject(k);
								String parameterName =  timeObj.getJSONObject("parameter").getString("parameterName");
								String parameterUnit = "";
								String unitStr = "";
								if(timeObj.getJSONObject("parameter").has("parameterUnit")){
									parameterUnit = timeObj.getJSONObject("parameter").getString("parameterUnit");
								}
								if("百分比".equals(parameterUnit)){
									unitStr = "%";
								}else if("C".equals(parameterUnit)){
									unitStr = "\u2103";
								}
								String startDateStr = "";
								String endDateStr = "";
								
								try {
									if(timeObj.has("startTime")){
										Date startD = sdf1.parse(timeObj.getString("startTime"));
										startDateStr = sdf2.format(startD);
									}
									if(timeObj.has("endTime")){
										Date endD = sdf1.parse(timeObj.getString("endTime"));
										endDateStr = sdf2.format(endD);
									}
								} catch (ParseException e) {
									e.printStackTrace();
								}
								
								sb.append("開始時間：" + startDateStr + br);
								sb.append("結束時間：" + endDateStr + br);
								sb.append(elementPre + parameterName + unitStr + br +br);
							}
							sb.append("-------------------------" + br + br);
						}
					}
				}
			}else{
				return WEATHER_ERROR;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	
	public static String weatherResult2Message(String resultStr){
		JSONObject json = null;
		StringBuffer sb = new StringBuffer();
		String br = "\r\n";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("M月d日  H:mm");
		try {
			json = new JSONObject(resultStr);
			JSONObject records = null;
			JSONArray location = null;
			if(json.has("success")){
				if("true".equals(json.getString("success"))){
					if(json.has("records")){
						records = json.getJSONObject("records");
					}else{
						return WEATHER_ERROR;
					}
					if(records.has("location")){
						location = records.getJSONArray("location");
					}else{
						return "嗯嗯？？找不到城市的資料耶，是不是飛到火星去啦？";
					}
					
					sb.append("天氣報告" +br +br);
					if(location.length() == 0){
						return "嗯嗯？？找不到城市的資料耶，是不是飛到火星去啦？";
					}
					for(int i = 0;i < location.length() ; i++){
						JSONObject item = location.getJSONObject(i);
						
						//取得城市名稱
						sb.append(item.getString("locationName") + br + br);
						//取得氣象資料
						WeatherVo wItem = new WeatherVo();
						JSONArray weatherAry = item.getJSONArray("weatherElement");
						for(int j = 0 ; j < weatherAry.length() ; j ++){
							JSONObject weatherItem = weatherAry.getJSONObject(j);
							String elementName = weatherItem.getString("elementName");
							JSONArray times = weatherItem.getJSONArray("time");
							JSONObject timeObj = times.getJSONObject(0);
							String parameterName =  timeObj.getJSONObject("parameter").getString("parameterName");
							String startDateStr = "";
							String endDateStr = "";
							
							try {
								if(timeObj.has("startTime")){
									Date startD = sdf1.parse(timeObj.getString("startTime"));
									startDateStr = sdf2.format(startD);
								}
								if(timeObj.has("endTime")){
									Date endD = sdf1.parse(timeObj.getString("endTime"));
									endDateStr = sdf2.format(endD);
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
							if(null == wItem.getStartDate()){
								wItem.setStartDate(startDateStr);
							}
							
							if(null == wItem.getEndDate()){
								wItem.setEndDate(endDateStr);
							}
							
							if("Wx".equalsIgnoreCase(elementName)){
								wItem.setWx(parameterName);
							}else if("PoP".equalsIgnoreCase(elementName)){
								wItem.setPop(parameterName);
							}else if("CI".equalsIgnoreCase(elementName)){
								wItem.setCi(parameterName);
							}else if("MinT".equalsIgnoreCase(elementName)){
								wItem.setMinT(parameterName);
							}else if("MaxT".equalsIgnoreCase(elementName)){
								wItem.setMaxT(parameterName);
							}
						}
						sb.append(wItem.getStartDate() + br);
						sb.append("         到"+br);
						sb.append(wItem.getEndDate() + br);
						sb.append("---------------------" + br );
						sb.append("氣溫：" + wItem.getMinT() + "\u2103 ~ " + wItem.getMaxT() + "\u2103" + br);
						sb.append("舒適度：" + wItem.getCi()+ br);
						sb.append("天氣現象：" + wItem.getWx()+ br);
						sb.append("降雨機率：" + wItem.getPop() + "%"+ br);
						sb.append("---------------------" + br + br);
					}
				}
			}else{
				return WEATHER_ERROR;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
}
