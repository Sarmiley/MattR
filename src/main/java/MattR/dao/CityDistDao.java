package MattR.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import MattR.application.LineBotAppApplication;
import MattR.data.CityDistVo;

@Repository
public class CityDistDao{
	
	
	public List<Map<String,Object>> selectCityDistByCity(String city){
		
		List<Map<String,Object>> cityList;
		String cityLike = city + "%";
		
		System.err.println("====== 抓取查詢縣市  ====== cityLike = " + cityLike);
		System.err.println("jdbcTemplate = " + LineBotAppApplication.jdbcTemplate);
		
		String sql = "SELECT SERIAL,API_CODE,CITY_NAME,DIST_NAME FROM heroku_fc94fd904c17c68.city_dist WHERE CITY_NAME LIKE '" + cityLike +"'";
		cityList = LineBotAppApplication.jdbcTemplate.queryForList(sql);
		System.err.println("cityList count = " + cityList.size());
		return cityList;
	}
	
	
	public List<CityDistVo> selectCityDistByCity2(String city) throws SQLException{
		List<CityDistVo> cityList2 = new ArrayList<CityDistVo>();
		Connection conn = LineBotAppApplication.jdbcTemplate.getDataSource().getConnection();
		
		String sql = "SELECT SERIAL,API_CODE,CITY_NAME,DIST_NAME FROM heroku_fc94fd904c17c68.city_dist WHERE CITY_NAME LIKE ? ";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, city);
		
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()){
			CityDistVo vo = new CityDistVo();
			vo.setSerial(rs.getInt("SERIAL"));
			vo.setCityName(rs.getString("CITY_NAME"));
			vo.setApiCode(rs.getString("API_CODE"));
			vo.setDistName(rs.getString("DIST_NAME"));
			
			cityList2.add(vo);
		}

		
		return cityList2;
	}
	
	public int insertCityDistByCity2(CityDistVo vo) throws SQLException{
		Connection conn = LineBotAppApplication.jdbcTemplate.getDataSource().getConnection();
		int count =0;
		try{
			String sql = "INSERT INTO heroku_fc94fd904c17c68.city_dist VALUES(?,?,?,?,?) ";
	
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, vo.getSerial());
			ps.setString(2, vo.getApiCode());
			ps.setString(3, vo.getCityName());
			ps.setString(4, vo.getDistName());
			ps.setString(5, vo.getZipCode());
			
			count = ps.executeUpdate();
		
		}catch(Exception e){
			System.err.println("insertCityDistByCity2 error : " + e.getLocalizedMessage());
			return 0;
		}
		
		return count;
	}
	
	

}
