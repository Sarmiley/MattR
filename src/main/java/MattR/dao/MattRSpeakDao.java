package MattR.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import MattR.application.LineBotAppApplication;
import MattR.data.MattRSpeakVo;

@Repository
public class MattRSpeakDao{
	
	public int insertSpeak(MattRSpeakVo vo) {
		int successCount = 0;
		try {
			String msgKey = vo.getMsgKey();
			String replyMsg = vo.getReplyMsg();
			String sql = "INSERT INTO heroku_fc94fd904c17c68.matt_r_speak (msg_key,reply_msg) VALUES (?, ?)";
			successCount = LineBotAppApplication.jdbcTemplate.update(sql,msgKey,replyMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return successCount;
	}
	
	
	public String queryReplyMsg(String keyword){
		String replyMsg = "聽不懂你說什麼，可以教我嗎？";
		
		String sql = "SELECT reply_msg FROM heroku_fc94fd904c17c68.matt_r_speak WHERE MSG_STATUS = 1 AND MSG_KEY = '" + keyword + "'";
		List<Map<String,Object>> replyMsgList = LineBotAppApplication.jdbcTemplate.queryForList(sql);
		
		if(replyMsgList!= null){
			if(replyMsgList.size() > 0 ){
				replyMsg = replyMsgList.get(0).get("reply_msg").toString();
			}
		}else{
			System.err.println("========== DB中搜尋不到關鍵字  keyword:"+ keyword +"  ===========");
		}
		return replyMsg;
	}
	
	public int setMsgStatus(String keyword){
		String sql = "UPDATE heroku_fc94fd904c17c68.matt_r_speak SET MSG_STATUS = 0 WHERE MSG_KEY = '" + keyword + "'";
		int count = LineBotAppApplication.jdbcTemplate.update(sql);
		return count;
	}
	
}
