package MattR.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import MattR.data.ProductsVo;

public class ProductsDao {

	public List<ProductsVo> queryAll() {
		List<ProductsVo> prodList = null;
		
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/testjdbc";
		String user = "sarmiley";
		String password = "d93780628";

		Connection conn = null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);

			
			String sql = "select PROD_ID , PROD_NAME , PROD_QTY , PROD_PRICE  from  PRODUCTS ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			prodList = new ArrayList<ProductsVo>();
			
			while(rs.next()){
				ProductsVo item = new ProductsVo();
				
				item.setProdId(rs.getString("PROD_ID"));
				item.setProdName(rs.getString("PROD_NAME"));
				item.setProdPrice(rs.getInt("PROD_QTY"));
				item.setProdQty(rs.getInt("PROD_PRICE"));
				
				prodList.add(item);
			}
			
		} catch (ClassNotFoundException e) {
			System.out.println("找不到驅動程式類別 : " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL相關錯誤  : " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println("conn 沒關好啦  : " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}

		return prodList;
	}
	
	
	public List<ProductsVo> queryProductsByProdName(String enterName) {
		List<ProductsVo> prodList = null;
		
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/testjdbc";
		String user = "sarmiley";
		String password = "d93780628";

		Connection conn = null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);

			String sql = "select PROD_ID , PROD_NAME , PROD_QTY , PROD_PRICE  from  PRODUCTS  where PROD_NAME = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, enterName);
			
			
			ResultSet rs = ps.executeQuery();
			
			prodList = new ArrayList<ProductsVo>();
			
			while(rs.next()){
				ProductsVo item = new ProductsVo();
				
				item.setProdId(rs.getString("PROD_ID"));
				item.setProdName(rs.getString("PROD_NAME"));
				item.setProdPrice(rs.getInt("PROD_QTY"));
				item.setProdQty(rs.getInt("PROD_PRICE"));
				
				prodList.add(item);
			}
			
		} catch (Exception e) {
			System.out.println("queryProductsByProdName error : " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println("conn 沒關好啦  : " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}

		return prodList;
	}

}
