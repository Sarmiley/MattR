package MattR.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MattFitnessTest {
	
	
	@RequestMapping(value="/MattFitnessTest", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView hello() throws Exception{
		ModelAndView modelAndView =  new ModelAndView("/MattFitnessTest");
	    return modelAndView;
	}
	
	@RequestMapping(value="/googlec0a5772df33c7138.html", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView googleHtml() throws Exception{
		ModelAndView modelAndView =  new ModelAndView("/googlec0a5772df33c7138.html");
	    return modelAndView;
	}
	
}