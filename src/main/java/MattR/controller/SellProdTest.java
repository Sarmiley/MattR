package MattR.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import MattR.dao.ProductsDao;
import MattR.data.ProductsVo;

public class SellProdTest {
 
	 public static List<String> productsList;
	 public static Map<String,Map<String,Integer>> productsInfo;
 
 
 /*
  * 題目 = 
  * 1.輸入商品名稱，檢查 productsList 裡面有沒有這項商品，沒有這項商品就直接印     "查無此商品" ， 找不到商品就不往下做
  * 2.如果productsList裡面有商品，把商品資訊取出來，然後比較剩餘數量還夠不夠他買   (例: PROD_QTY = 2 ， 他要買4個 ) 就印出 "商品庫存量不足"，數量不足就不往下做
  * 3.如果有此商品，庫存量也夠，就印出  商品名稱 、 商品單價、購買數量、總價    (例:  商品名稱:立可白    商品單價:25元   購買數量:5個   總價： 125元)
  * 
  * 如果打數量的時候，數字鍵盤打不出來，就用上面那排打(ㄅㄉˇ ˋ ㄓˊ ˙ ㄚ ㄞ)
  */
	private static ProductsDao productsDao;
	
	public static void main(String[] args){
	 Scanner sc =new Scanner(System.in);
	 System.err.println("請輸入你要買的商品名稱：");
	 String enterProdName = sc.nextLine();
	 System.err.println("請輸入你要買的商品數量：");
	 int enterProdQTY = sc.nextInt();
	 System.err.println("您此次要購買的商品為:" + enterProdName + "，購買的數量為:" + enterProdQTY + "個");
	 
	 productsDao = new ProductsDao();
	 List<ProductsVo> voList = productsDao.queryProductsByProdName(enterProdName);
	 
	 
	 
	 
	 queryAll();             //印出全部商品資訊用(給你驗算用)
	}
 
 
	 /*
	  * 給你檢查裡面資料用
	  */
	public static void queryAll(){
		List<ProductsVo> voList = new ArrayList<ProductsVo>();
		 
		voList = productsDao.queryAll();
		System.err.println("  =====================  ");
		for(ProductsVo vo : voList){
			System.err.println("商品代號 : " + vo.getProdId());
			System.err.println("商品名稱 : " + vo.getProdName());
			System.err.println("商品價格 : " + vo.getProdPrice());
			System.err.println("商品數量 : " + vo.getProdQty());
			System.err.println("  =====================  ");
			 
		}
	}
}