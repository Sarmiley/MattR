/**
 * 功能選擇控制器
 * 接收入口傳入的事件輸入，指派下一步執行的方法
 */
package MattR.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;

import MattR.method.MethodsCheckController;

@Controller
public class LineBotAppController {
	
	private static MethodsCheckController mcc;
	
	public LineBotAppController() throws IOException{
		if(mcc == null){
			mcc = new MethodsCheckController();
		}
	}
	
	
	/**
	 * 若為訊息回復功能，則進入方法選擇
	 * 並回傳執行方法後的文字結果回傳
	 * @param inputMsg
	 * @return String
	 * @author Matt Liu
	 * @since 2018/08/16
	 * @verion 1.0
	 */
	public String replyMessage(String inputMsg){
		System.err.println("==========  replyMessage  =============");
		return mcc.methodCheck(inputMsg);
	}
	
	
	
	/**
	 * 若功能為加入群組時，執行的方法
	 * @param inputMsg
	 * @return String
	 * @author Matt Liu
	 * @since 2018/08/16
	 * @verion 1.0
	 */
	public String joinGroup(String inputMsg){
		return "麥特RRRRR，霸氣登場！";
	}
	

}
