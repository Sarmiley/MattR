package MattR.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

	@Autowired
	private LineBotAppController lbac;

	@RequestMapping(value = "/hello/{key}/{value}", method = RequestMethod.GET)
	@ResponseBody
	public String hello(@PathVariable(name = "key") String key, @PathVariable(name = "value") String value)
			throws Exception {

		String inputMsg = key + "~" + value;
		System.err.println(inputMsg);
		System.err.println(inputMsg);
		// String returnMsg = "學 花 哈 哈;花哈哈哈哈哈";
		// String returnMsg = "看資料夾 HUNTA";
		String returnMsg = lbac.replyMessage(inputMsg);
		// String returnMsg = sb.toString();

		return returnMsg;
	}

	@RequestMapping(value="/test_lambda", method=RequestMethod.GET)
	@ResponseBody
	public String test_lambda() throws Exception{
		
//		Stream.iterate(1, i -> i + 1).limit(9).forEach(i -> {
//			Stream.iterate(1, j -> j + 1).limit(i).forEach( j -> {
//				System.err.print( j + " * " + i + " = " + (i * j) + "\t");
//			});
//			System.err.println();
//		});
		
//		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
//
//		list.forEach((e) -> {
//			list.stream().filter((ee) -> ee <= e).forEach((eee) -> {
//				System.out.print(eee + "*" + e + "=" + eee * e + "; ");
//			});
//			System.out.println();
//		});
		StringBuffer sb = new StringBuffer();
		List<String> list = Arrays.asList("蘋果","楊桃","西瓜","草莓");
		list.forEach(s -> {
			sb.append(s);
		});
		
		
		System.err.println(sb.toString());
		return sb.toString();
	}

}