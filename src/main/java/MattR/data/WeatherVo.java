package MattR.data;

public class WeatherVo {
	private String wx;
	private String minT;
	private String maxT;
	private String pop;
	private String ci;
	private String startDate;
	private String endDate;
	public String getWx() {
		return wx;
	}
	public void setWx(String wx) {
		this.wx = wx;
	}
	public String getMinT() {
		return minT;
	}
	public void setMinT(String minT) {
		this.minT = minT;
	}
	public String getMaxT() {
		return maxT;
	}
	public void setMaxT(String maxT) {
		this.maxT = maxT;
	}
	public String getPop() {
		return pop;
	}
	public void setPop(String pop) {
		this.pop = pop;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	
}
