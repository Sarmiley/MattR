package MattR.data;

import org.springframework.stereotype.Component;

@Component
public class CityDistVo {

	private Integer serial;
	private String apiCode;
	private String cityName;
	private String distName;
	private String zipCode;
	
	public Integer getSerial() {
		return serial;
	}
	
	public void setSerial(Integer serial) {
		this.serial = serial;
	}
	
	public String getApiCode() {
		return apiCode;
	}
	
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	
	public String getCityName() {
		return cityName;
	}
	
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public String getDistName() {
		return distName;
	}
	
	public void setDistName(String distName) {
		this.distName = distName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	
}
