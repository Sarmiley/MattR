package MattR.data;

import org.springframework.stereotype.Component;

@Component
public class MattRSpeakVo {
	
	private Integer msgSerial;
	private String msgKey;
	private String replyMsg;
	private Integer msgStatus;
	
	
	public Integer getMsgSerial() {
		return msgSerial;
	}
	public void setMsgSerial(Integer msgSerial) {
		this.msgSerial = msgSerial;
	}
	public String getMsgKey() {
		return msgKey;
	}
	public void setMsgKey(String msgKey) {
		this.msgKey = msgKey;
	}
	public String getReplyMsg() {
		return replyMsg;
	}
	public void setReplyMsg(String replyMsg) {
		this.replyMsg = replyMsg;
	}
	public Integer getMsgStatus() {
		return msgStatus;
	}
	public void setMsgStatus(Integer msgStatus) {
		this.msgStatus = msgStatus;
	}
}
