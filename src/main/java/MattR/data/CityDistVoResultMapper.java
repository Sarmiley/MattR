package MattR.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CityDistVoResultMapper implements RowMapper<CityDistVo> {

	@Override
	public CityDistVo mapRow(ResultSet rs, int rowNum) throws SQLException {
		CityDistVo vo = new CityDistVo();
		for (int i = 0; i < rowNum; i++) {
			rs.next();
		}
		vo.setSerial(rs.getInt("serial"));
		vo.setApiCode(rs.getString("api_code"));
		vo.setCityName(rs.getString("city_name"));
		vo.setDistName(rs.getString("dist_name"));
		return vo;
	}

}
