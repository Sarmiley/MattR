package MattR.data;

import java.util.List;
import java.util.Map;

import MattR.dao.CityDistDao;

public class WeatherCityMap {
	
	
	//取得縣市資料
	public static String getCityData(String key){
		CityDistDao cityDistDao = new CityDistDao();
		key = key.replace("台","臺");
		List<Map<String,Object>> cityList = cityDistDao.selectCityDistByCity(key);
		if(cityList.size() > 0){
			Map<String,Object> item= cityList.get(0);
			if(item.containsKey("CITY_NAME")){
				key = item.get("CITY_NAME").toString();
			}
		}
		return key; 
	}
	
}
