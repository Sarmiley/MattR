package MattR.data;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "line.bot")
@ConditionalOnProperty(prefix = "line.bot", name = "enabled", matchIfMissing = true)
public class LineProperties {
    private boolean enabled = false;
    private String channelSecret;
    private String channelToken;
    private String to;
}