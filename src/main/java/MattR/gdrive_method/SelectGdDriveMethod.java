package MattR.gdrive_method;

import java.io.IOException;
import java.util.List;

import com.google.api.services.drive.model.File;

import MattR.method.CallGoogleDriveApi;

public class SelectGdDriveMethod {
	
	private static List<File> driveFolderRootList;
	private static File manRomance;
	
	
	static{
		if(driveFolderRootList == null){
			init();
			for(File file : driveFolderRootList){
				if("男人的浪漫".equals(file.getName())){
					manRomance = file;
				}
			}
//			System.err.println(manRomance.getName() + ";" + manRomance.getId());
		}
	}
	public SelectGdDriveMethod() throws IOException{
		if(driveFolderRootList == null){
			this.init();
			for(File file : driveFolderRootList){
				if("男人的浪漫".equals(file.getName())){
					manRomance = file;
				}
			}
			System.err.println(manRomance.getName() + ";" + manRomance.getId());
		}
	}
	
	private static void init(){
		driveFolderRootList = CallGoogleDriveApi.getDriveFolderMap(null);
	}
	
	//用資料夾名稱取得Google Drive取的第一層資料夾
	public static String getTopNodeFolderIdByName(String name){
		for(File file : driveFolderRootList){
			if(name.equals(file.getName())){
				return file.getId();
			}
		}
		return "";
	}
	
	
	public String getDriveRootFolderMap(){
		StringBuffer sb = new StringBuffer();
		for(File file : driveFolderRootList){
			sb.append("ID：" + file.getId() + " ； 資料夾名稱："+ file.getName() + "\r\n");
		}
		return sb.toString();
	}
	
	
	//利用父資料夾ID取得子資料夾清單
	public String getDriveFolderList(String parentId){
		StringBuffer sb = new StringBuffer();
		List<File> folderList = CallGoogleDriveApi.getDriveFolderMap(parentId);
		for(File file : folderList){
			sb.append(file.getName() + "\r\n");
		}
		return sb.toString();
	}
	
	//利用資料夾名稱取得資料夾ID
	public String getFolderIdByName(String folderName){
		if(folderName == null){
			return null;
		}
		return CallGoogleDriveApi.getDriveFolderByName(folderName);
	}
	
	//利用資料夾名稱取得檔案清單
	public String getFilesListByFolderName(String folderName ,String ext){
		StringBuffer sb = new StringBuffer();
		String folderId = getFolderIdByName(folderName);
		if("".equals(folderId)){
			return "沒有這個資料夾好嗎？";
		}
		List<File> filesList = CallGoogleDriveApi.getFilesListByFolderName(folderId , ext);
		if(filesList.size() > 0){
			for(File file : filesList){
				sb.append(file.getName() + "\r\n");
			}
		}else{
			sb.append("這個資料夾裡面沒有檔案。");
		}
		return sb.toString();
	}
	
	//利用關鍵字跟限制筆數來查詢
	@SuppressWarnings("null")
	public String getAVFilesContent(String keyword,String folder,String ext,String size){
		StringBuffer sb = new StringBuffer();
		List<File> filesList = null;
		if(size == null){
			filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, null);
		}else{
			if(size.matches("[0-9]")){
				filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, size);
			}else{
				filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, null);
			}
		}
		if(filesList.size() > 0){
			for(File file : filesList){
				sb.append(file.getName() + "\r\n");
				sb.append(file.getWebViewLink() + "\r\n");
				sb.append("==========\r\n");
			}
		}else{
			sb.append("這個關鍵字找不到檔案。");
		}
		return sb.toString();
	}
	
	//利用關鍵字跟限制筆數來查詢
		@SuppressWarnings("null")
		public String getAVFilesList(String keyword,String folder,String ext,String size){
			StringBuffer sb = new StringBuffer();
			List<File> filesList = null;
			if(size == null){
				filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, null);
			}else{
				if(size.matches("[0-9]")){
					filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, size);
				}else{
					filesList = CallGoogleDriveApi.getAVFilesContent(keyword ,getFolderIdByName(folder),ext, null);
				}
			}
			if(filesList.size() > 0){
				for(File file : filesList){
					sb.append(file.getName() + "\r\n");
				}
			}else{
				sb.append("這個關鍵字找不到檔案。");
			}
			return sb.toString();
		}
}
