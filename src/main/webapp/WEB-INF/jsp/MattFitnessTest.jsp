<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Matt Fitness Test</title>
<link rel="stylesheet" href="/css/btn.css" />
<script src="https://apis.google.com/js/api.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
  function authenticate() {
    return gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/fitness.activity.read https://www.googleapis.com/auth/fitness.activity.write https://www.googleapis.com/auth/fitness.blood_glucose.read https://www.googleapis.com/auth/fitness.blood_glucose.write https://www.googleapis.com/auth/fitness.blood_pressure.read https://www.googleapis.com/auth/fitness.blood_pressure.write https://www.googleapis.com/auth/fitness.body.read https://www.googleapis.com/auth/fitness.body.write https://www.googleapis.com/auth/fitness.body_temperature.read https://www.googleapis.com/auth/fitness.body_temperature.write https://www.googleapis.com/auth/fitness.location.read https://www.googleapis.com/auth/fitness.location.write https://www.googleapis.com/auth/fitness.nutrition.read https://www.googleapis.com/auth/fitness.nutrition.write https://www.googleapis.com/auth/fitness.oxygen_saturation.read https://www.googleapis.com/auth/fitness.oxygen_saturation.write https://www.googleapis.com/auth/fitness.reproductive_health.read https://www.googleapis.com/auth/fitness.reproductive_health.write"})
        .then(function() { console.log("Sign-in successful"); },
              function(err) { console.error("Error signing in", err); });
  }
  function loadClient() {
    gapi.client.setApiKey("AIzaSyDcZNqeYPEICM4XfgFe27vWgg1l5jBXmas");
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/fitness/v1/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }
  // Make sure the client is loaded and sign-in is complete before calling this method.
  function execute() {
    return gapi.client.fitness.users.dataSources.list({
      "userId": "me"
    })
        .then(function(response) {
                // Handle the results here (response.result has the parsed body).
                console.log(response);
                $("#result").text(JSON.stringify(response));
              },
              function(err) { console.error("Execute error", err); });
  }
  gapi.load("client:auth2", function() {
    gapi.auth2.init({client_id: "648534655374-dqqpibcal8m9aqi7s077mq41fjnfcien.apps.googleusercontent.com"});
  });
</script>
</head>
<body>



<div style="margin:50px auto;text-align: center;">
<div class="ui-button"  onclick="authenticate().then(loadClient)">
	<div class="ui-button__frame">
		<span class="ui-button__frame--top"></span>
		<span class="ui-button__frame--left"></span>
		<span class="ui-button__frame--right"></span>
		<span class="ui-button__frame--bottom"></span>
	</div>
	<div class="ui-button__content">
		<div class="ui-button__background">
			<div class="ui-button__hover-out"></div>
			<canvas width="210" height="48" style="width: 210px; height: 48px;"></canvas>
		</div>
		<div class="ui-button__text">
			<span>authorize and load</span>
		</div>
	</div>
</div>
<div class="ui-button"  onclick="execute()">
	<div class="ui-button__frame">
		<span class="ui-button__frame--top"></span>
		<span class="ui-button__frame--left"></span>
		<span class="ui-button__frame--right"></span>
		<span class="ui-button__frame--bottom"></span>
	</div>
	<div class="ui-button__content">
		<div class="ui-button__background">
			<div class="ui-button__hover-out"></div>
			<canvas width="210" height="48" style="width: 210px; height: 48px;"></canvas>
		</div>
		<div class="ui-button__text">
			<span>execute</span>
		</div>
	</div>

<div style="margin:50px auto;text-align: center;">
	<span id="result">
	</span>
</div>

</body>
</html>